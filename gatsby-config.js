module.exports = {
  siteMetadata: {
    title: `SHELBY`,
    description: `Multiservisos automotriz, calle septima cucuta, villa del rosario`,
    author: `@JOKI_DEV`,
  },

  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: 'gatsby-plugin-antd',
      options: {
        style: true,
      },
    },
    {
      resolve: 'gatsby-plugin-less',
      options: {
        javascriptEnabled: true,
        modifyVars: {
          'primary-color': '#f2e30c',
          'font-family': 'limelight',
          'layout-body-background': 'linear-gradient(to top, #000046, #1cb5e0)',
        },
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /\.inline\.svg$/,
          options: {
            tag: 'symbol',
            name: 'MyIcon',
            props: {
              className: 'my-class',
              title: 'example',
            },
            filters: [value => console.log(value)],
          },
        },
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Roboto`,
          `source sans pro\:300,400,400i,700`, // you can also specify font weights and styles
        ],
        display: 'swap',
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
  ],
}
