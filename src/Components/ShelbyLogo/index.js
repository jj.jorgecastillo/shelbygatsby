/* eslint-disable jsx-a11y/heading-has-content */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'

import Icon from '@ant-design/icons'

import { ReactComponent as Logo } from '../../assets/images/shelby.svg'

const LogoIcon = props => <Icon component={Logo} {...props} />

const ShelbyLogo = ({ height }) => (
  <div style={{ height: `${height}` }}>
    <LogoIcon style={{ fontSize: '64px' }} />
  </div>
)

export default ShelbyLogo
