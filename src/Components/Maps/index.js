import React, { useState } from 'react'
import { Modal, Button, Grid } from 'antd'

import './Maps.less'

const responsiveWidths = {
  xs: '95%',
  sm: '95%',
  md: 762 - 32,
  lg: 762 + 100,
  xl: 992,
}
const url =
  'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d293.77986270873964!2d-72.46665275237874!3d7.830515396027133!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e664767b6e21e87%3A0x819ca274886b5569!2sMultiservicios%20automotriz%20shelby!5e0!3m2!1ses!2sco!4v1607376832829!5m2!1ses!2sco'

const Map = () => {
  const breakpoints = Grid.useBreakpoint()
  const [visible, setVisible] = useState(false)

  const getResponsiveWidth = ({ sm, md, lg, xl }) => {
    if (xl) return responsiveWidths.xl
    if (lg) return responsiveWidths.lg
    if (md) return responsiveWidths.md
    if (sm) return responsiveWidths.sm
    return responsiveWidths.xs
  }

  return (
    <div>
      <Button onClick={() => setVisible(true)} size="middle" shape="round">
        Ver mapa
      </Button>
      <Modal
        className="Maps"
        centered
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}
        width={getResponsiveWidth(breakpoints)}
        closable={false}
        footer={null}
      >
        <iframe
          title="Calle Septima Cl. 7 #3-213"
          src={url}
          width={getResponsiveWidth(breakpoints)}
          height="400"
          frameborder="0"
          style={{
            borderRadius: '10px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          allowfullscreen=""
          aria-hidden="false"
        ></iframe>
      </Modal>
    </div>
  )
}

export default Map
