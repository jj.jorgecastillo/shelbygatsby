import React from 'react'

import { Col } from 'antd'

const IconLink = ({ icon, text, href, xs = 12 }) => {
  return (
    <Col xs={xs} style={{ display: 'flex', justifyContent: 'center' }}>
      <a href={href} target="_blank" rel="noreferrer">
        <div
          style={{
            textAlign: 'center',
            fontSize: '24px',
            textSshadow: '0 2px 17px',
            padding: '8px',
          }}
        >
          {icon} {text}
        </div>
      </a>
    </Col>
  )
}

export default IconLink
