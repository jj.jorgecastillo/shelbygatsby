import React from 'react'

import { navigate } from 'gatsby'

import { Form, Input, Row, Col, Button, notification } from 'antd'
import { FrownTwoTone, SmileOutlined } from '@ant-design/icons'

const validateMessages = {
  required: '${label} Es requerido!', // eslint-disable-line
  types: {
    email: '${label} No es un correo válido!', // eslint-disable-line
    number: '${label} Solo se permiten números!', // eslint-disable-line
  },
}

const { TextArea } = Input

const FormContact = () => {
  const [form] = Form.useForm()

  //   const foo = document.getElementById('foo')
  //   foo.addEventListener('submit', (e) => {
  //   e.preventDefault()
  // const formData = new FormData(foo)
  // formData.append('userId', 123)
  //   fetch('//example.com', {
  //     method: 'POST',
  //     body: formData
  //   })
  // })

  // async function postData(url = '', values = {}) {
  //   const { name, email, phone, car, message } = values
  //   console.log(values)
  //   const formData = new FormData()
  //   formData.append('name', name)
  //   formData.append('email', email)
  //   formData.append('phone', phone)
  //   formData.append('car', car)
  //   formData.append('message', message)

  //   // Default options are marked with *
  //   const response = await fetch(url, {
  //     method: 'POST',
  //     mode: 'cors',
  //     headers: {
  //       ContentType: 'application/multipart/form-data',
  //     },
  //     body: formData,
  //   })
  // }

  const onFinish = values => {
    const formData = new FormData()

    const { name, email, phone, car, message } = values

    formData.append('name', name)
    formData.append('email', email)
    formData.append('phone', phone)
    formData.append('car', car)
    formData.append('message', message)
    formData.append('_template', 'table')
    formData.append('_captcha', false)
    formData.append('_cc', 'jj.jorgecastillo@gmail.com')

    fetch('https://formsubmit.co/tallershelby@gmail.com', {
      method: 'POST',
      mode: 'no-cors',
      headers: {
        ContentType: 'application/multipart/form-data',
        // ContentType: 'application/x-www-form-urlencoded',
      },
      body: formData,
    })
      .then(() => {
        notification.open({
          type: 'success',
          icon: <SmileOutlined style={{ color: '#108ee9' }} />,
          message: 'Mensaje enviado con éxito',
          duration: 5,
          description: `${name} Pronto nos comunicaremos contigo.`,
        })
        form.resetFields()
        navigate('/promotions')
      })
      .catch(() => {
        notification.open({
          type: 'error',
          icon: <FrownTwoTone twoToneColor="#eb2f96" />,
          message: 'Mensaje No enviado ',
          duration: 5,
          description: `${name} Ocurrio un error, intenta nuevamente.`,
        })
      })
  }

  return (
    <section>
      <Form form={form} onFinish={onFinish} validateMessages={validateMessages} layout="vertical">
        <Row gutter={[16, 16]}>
          <Col xs={24} sm={12}>
            <Form.Item name="name" label="Nombre" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={12}>
            <Form.Item name="email" label="Email" rules={[{ required: true, type: 'email' }]}>
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={12}>
            <Form.Item name="phone" label="Teléfono" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={12}>
            <Form.Item name="car" label="Vehiculo">
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24}>
            <Form.Item name="message" label="Mensaje" rules={[{ required: true }]}>
              <TextArea
                className="field"
                autoSize={{ minRows: 3, maxRows: 4 }}
                placeholder="Escribe tu consulta que con gusto te responderemos"
                showCount
                maxLength={200}
              />
            </Form.Item>
          </Col>
          <Col xs={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Form.Item>
              <Button size="large" shape="round" htmlType="submit">
                Enviar mensaje
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </section>
  )
}

export default FormContact

//  <section id="contacto" className="wrapper style1 fade-up">
//         <div className="inner">
//           <h2>COMUNÍCATE CON NOSOTROS</h2>
//           <p> </p>
//           <div className="split style1">
//             <section>
//               <form
//                 target="_blank"
//                 action="https://formsubmit.co/tallershelby@gmail.com"
//                 method="POST"
//               >
//                 <input
//                   type="hidden"
//                   name="_next"
//                   value="https://shelby-alpha.vercel.app/promociones.html"
//                 />
//                 <input type="hidden" name="_captcha" value="false" />
//                 <input
//                   type="hidden"
//                   name="_autoresponse"
//                   value="Hola recibimos tu correo pronto nos comunicaremos contigo. Att: tallershelby"
//                 />
//                 <input type="hidden" name="_cc" value="egrojese@gmail.com" />
//                 <input type="hidden" name="_template" value="box" />
//                 <div className="fields">
//                   <div className="field half">
//                     <label htmlFor="name">
//                       <small style={{ color: '#fff836' }}> * </small>Nombre
//                     </label>

//                     <input type="text" name="name" id="name" required placeholder="Nombre" />
//                   </div>
//                   <div className="field half">
//                     <label htmlFor="email">
//                       <small style={{ color: '#fff836' }}> * </small>Email
//                     </label>
//                     <input
//                       type="email"
//                       name="email"
//                       id="email"
//                       required
//                       placeholder="email@email.com"
//                     />
//                   </div>
//                   <div className="field half">
//                     <label htmlFor="phone">
//                       <small style={{ color: '#fff836' }}> * </small>Celular{' '}
//                     </label>
//                     <input
//                       type="tel"
//                       id="phone"
//                       name="phone"
//                       pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
//                       required
//                       placeholder="123-4567-8901"
//                     />

//                     <small>Formato 123-456-7890</small>
//                     <span className="validity"></span>
//                   </div>
//                   <div className="field half">
//                     <label htmlFor="vehicle">Vehículo</label>
//                     <input type="text" name="vehicle" id="phone" placeholder="modelo" />
//                   </div>
//                   <div className="field">
//                     <label htmlFor="message">
//                       <small style={{ color: '#fff836' }}> * </small>Mensaje
//                     </label>
//                     <textarea
//                       name="message"
//                       id="message"
//                       rows="4"
//                       maxLength="150"
//                       placeholder="Escribe tu consulta que con gusto te responderemos"
//                       required
//                     />
//                   </div>
//                 </div>
//                 <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '2rem' }}>
//                   <a href="#one" className="scrolly">
//                     <Button size="large" shape="round" htmlType="submit">
//                       Enviar mensaje
//                     </Button>
//                   </a>
//                 </div>
//               </form>
//             </section>

//             <FormContact />

//             <section>
//               <ul className="contact">
//                 <li>
//                   <h3>Dirección</h3>
//                   <span>
//                     Colombia, Norte de Santander
//                     <br />
//                     Villa del Rosario
//                     <br />
//                     Calle Septima Cl. 7 #3-213
//                   </span>
//                 </li>
//                 <li>
//                   <div style={{ display: 'flex', justifyContent: 'center' }}>
//                     <Map />
//                   </div>
//                 </li>
//                 {/* <li>
//                   <h3>Email</h3>
//                   <a href="#">tallershelby@gmail.com</a>
//                   <a href="#">multiserviciosautomotrizshelby@gmail.com</a>
//                 </li>
//                   */}
//                 <li>
//                   <h3>Teléfono</h3>
//                   <IconLink
//                     xs={24}
//                     icon={<WhatsAppOutlined />}
//                     text="314 4242856"
//                     href="https://wa.link/h3bwpq"
//                   />
//                 </li>
//               </ul>
//               <SocialMedia />
//             </section>
//           </div>
//         </div>
//       </section>
