import React from 'react'

import { Layout } from 'antd'

const { Footer } = Layout

const FooterShelby = () => {
  return (
    <Footer>
      <h4>
        Designed by <strong>JOKI_DEV</strong>
      </h4>
    </Footer>
  )
}

export default FooterShelby
