import React from 'react'

import { Layout } from 'antd'

const { Content } = Layout

const ContentShelby = ({ children }) => {
  return <Content>{children}</Content>
}

export default ContentShelby
