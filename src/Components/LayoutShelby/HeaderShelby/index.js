import React from 'react'

import { Layout, Grid } from 'antd'

import doble from '../../../assets/images/doble.svg'

import septima from '../../../assets/images/septima.svg'

const { Header } = Layout

const { useBreakpoint } = Grid

const HeaderShelby = () => {
  const breakpoints = useBreakpoint()

  return (
    <Header>
      <img src={breakpoints.sm ? doble : septima} alt="Logo septima" width="100%" />
    </Header>
  )
}

export default HeaderShelby
