import React, { useState } from 'react'

import { Link } from 'gatsby'

import { Divider, Layout, Menu, Avatar, Grid } from 'antd'

import logo from '../../../assets/images/shelby.svg'

import { menuData } from '../../../data'

const { Sider } = Layout

const { Item } = Menu

const { useBreakpoint } = Grid

const SiderMenuShelby = ({ children }) => {
  const breakpoints = useBreakpoint()

  const [collapsed] = useState(false)

  const menuItem = item => {
    const { key, icon: Icon, text } = item
    return (
      <>
        <Item key={key}>
          <Icon twoToneColor="#1cb5e0" style={{ fontSize: 24 }} />
          <Link to={`/${key}`} activeClassName="selected">
            {text}
          </Link>
        </Item>
        <Divider />
      </>
    )
  }

  return (
    <Sider
      trigger={null}
      collapsible
      collapsed={breakpoints.sm ? collapsed : !collapsed}
      style={{
        height: '100vh',
        position: 'sticky',
        zIndex: 1,
        top: '0',
        flex: 1,
        maxWidth: '10rem',
        minWidth: '10rem',
        width: '200px',
      }}
    >
      <div className="logo">{breakpoints.sm ? <Divider /> : <Avatar size={80} src={logo} />}</div>

      <Menu mode="inline">
        <Divider />
        {menuData.map(item => menuItem(item))}
      </Menu>
    </Sider>
  )
}

export default SiderMenuShelby
