import React from 'react'

import { BackTop, Layout } from 'antd'

import { Helmet } from 'react-helmet'

import FooterShelby from './FooterShelby'
import HeaderShelby from './HeaderShelby'
import SiderMenuShelby from './SiderMenuShelby'
import ContentShelby from './ContentShelby'

import '../../assets/less/App.less'

const LayoutShelby = ({ children }) => {
  return (
    <Layout className="App">
      <Helmet>
        <meta charSet="utf-8" />
        <title>Multiservicios Shelby</title>
        <link rel="canonical" href="http://mysite.com/example" />
      </Helmet>
      <BackTop />
      <SiderMenuShelby />
      <Layout>
        <HeaderShelby />
        <ContentShelby>{children}</ContentShelby>
        <FooterShelby />
      </Layout>
    </Layout>
  )
}

export default LayoutShelby
