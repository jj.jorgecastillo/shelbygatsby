import React from 'react'

import { Row, Col, Image, Divider } from 'antd'

import {
  FacebookFilled,
  InstagramFilled,
  TwitterCircleFilled,
  YoutubeFilled,
  WhatsAppOutlined,
} from '@ant-design/icons'

import logo from '../../assets/images/shelby.svg'
import qrw from '../../assets/images/qrw.png'
import IconLink from '../IconLink'
import Map from '../Maps'

const socialContent = (
  <Row gutter={8}>
    <IconLink
      icon={<InstagramFilled />}
      text="Instagram"
      href="https://www.instagram.com/tallershelbyoficial/"
    />
    <IconLink
      icon={<FacebookFilled />}
      text="Facebook"
      href="https://www.facebook.com/tallershelby/"
    />
    <IconLink
      icon={<TwitterCircleFilled />}
      text="Twitter"
      href="https://twitter.com/tallershelbyof1"
    />
    <IconLink
      icon={<YoutubeFilled />}
      text="Youtube"
      href="https://www.youtube.com/channel/UCj_wz2UWe5lCVoNVrO6fzqg?view_as=subscriber"
    />
  </Row>
)

const SocialMedia = () => {
  return (
    <section>
      <ul className="contact">
        <li>
          <h3>Dirección</h3>
          <span>
            Colombia, Norte de Santander
            <br />
            Villa del Rosario
            <br />
            Calle Septima Cl. 7 #3-213
          </span>
        </li>
        <li>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Map />
          </div>
        </li>
        <li>
          <h3>Teléfono</h3>
          <IconLink
            xs={24}
            icon={<WhatsAppOutlined />}
            text="314 4242856"
            href="https://wa.link/h3bwpq"
          />
        </li>
      </ul>
      <Row gutter={8}>
        <Col xs={18} style={{ display: 'flex', justifyContent: 'center' }}>
          <Image src={logo} alt="Logo Shleby" width="100%" />
        </Col>
        <Col xs={6} style={{ display: 'flex', alignItems: 'flex-end' }}>
          <Image src={qrw} alt="qrw Shleby" width="50%" />
        </Col>
        {socialContent}
        <Divider />
      </Row>
    </section>
  )
}

export default SocialMedia
