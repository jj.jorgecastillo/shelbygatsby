import React from 'react'

import { Button, Divider, Image } from 'antd'

import carro from './../../assets/images/carro.svg'
import equipo from './../../assets/images/equipo.jpg'
import herramientas from './../../assets/images/herramientas.jpg'

import SocialMedia from '../SocialMedia'

import './../../assets/less/app.less'
import FormContact from '../FormContact'

const shelbyValues = (title, subTitle) => {
  return (
    <li>
      <div style={{ display: 'flex', alignItems: 'baseline' }}>
        <h3>{title}</h3>
        <span> {subTitle}</span>
      </div>
    </li>
  )
}

export default function MainShelby() {
  return (
    <>
      <section className="wrapper style1 fullscreen fade-up">
        <div style={{ textAlign: 'center' }}>
          <h1>SHELBY</h1>
          <h2>Multi-Servicios Automotriz</h2>
          <Divider />
          <h2>Lunes a Sábados</h2>
          <h3>08:00 AM - 06:00 PM</h3>

          <div style={{ display: 'flex', justifyContent: 'center', marginTop: '4yaerrem' }}>
            <a href="#one" className="scrolly">
              <Button size="large" shape="round">
                VER MÁS
              </Button>
            </a>
          </div>
        </div>
      </section>
      <section className="wrapper style1 fade-up">
        <div id="one" className="inner">
          <div className="split style1">
            <section>
              <h2>EXPERIENCIA</h2>
              <p>
                Trabajamos bajo parámetros de excelencia,
                <br />
                seriedad, calidad, garantía y cumplimiento <br />
                en la ejecución de todos nuestros servicios.
              </p>
              <div className="image">
                <Image
                  src={carro}
                  alt="imagen carro shelby"
                  width="100%"
                  data-position="center center"
                />
              </div>
            </section>
            <section>
              <ul className="contact">
                <h2>Valores</h2>
                {shelbyValues('S', ' eguridad')}
                {shelbyValues('H', ' onestidad')}
                {shelbyValues('E', ' tica')}
                {shelbyValues('L', ' ealtad')}
                {shelbyValues('B', ' ondad')}
                {shelbyValues(
                  'Y',
                  '  por eso y más debes elegirnos como la mejor opción para tu vehículo.',
                )}
              </ul>
            </section>
          </div>
        </div>
      </section>
      <section id="two" className="wrapper style3 fade-up">
        <div className="inner">
          <h2>Que Hacemos</h2>
          <p>
            Mecánica general, Repuestos, Electrónica Automotriz, Frenos, Suspensión, Alineación,
            Balanceo, Montallantas, Sincronización, Aire Acondicionado, Lavado y Mantenimiento en
            General.
          </p>
          <div className="features">
            <section>
              <h3>Nos mantenemos a la Vanguardia con Equipos Modernos</h3>
              <p>
                Contamos con herramientas de primera para un diagnóstico acertado en su vehículo no
                dudes en contactarnos.
              </p>
              <Image width="100%" height="100" src={herramientas} data-position="25% 25%" />
            </section>
            <section>
              <h3>Personal altamente capacitado.</h3>
              <p>
                No dudes en hacernos las consultas para el mantenimiento y mejoramiento de tu
                vehículo, nuestra meta es brindar un servicio de excelencia a nuestros clientes.
              </p>
              <Image
                width="100%"
                height="100"
                src={equipo}
                alt="equipo shelby"
                data-position="25% 25%"
              />
            </section>
          </div>
        </div>
      </section>
      <section id="contacto" className="wrapper style1 fade-up">
        <div className="inner">
          <h2>COMUNÍCATE CON NOSOTROS</h2>
          <p> </p>
          <div className="split style1">
            <FormContact />
            <SocialMedia />
          </div>
        </div>
      </section>
    </>
  )
}
