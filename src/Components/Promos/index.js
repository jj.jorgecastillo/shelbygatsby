import React from 'react'

import { Card, Image } from 'antd'

import { weeklyPromotion } from '../../data'

const { Meta } = Card

const gridStyle = {
  width: '25%',
  textAlign: 'center',
}

const dataPromo = (data = []) => (
  <>
    {data.map(({ id, label, discount, src }) => (
      <Card.Grid id={id} style={gridStyle}>
        <Card hoverable cover={<Image width={200} src={src} />}>
          <Meta title="label" description="discount" />
        </Card>
      </Card.Grid>
    ))}
  </>
)

const Promos = () => {
  return (
    <section>
      <Card title="Card Title">{dataPromo(weeklyPromotion)}</Card>
    </section>
  )
}

export default Promos
