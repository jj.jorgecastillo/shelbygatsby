import { ShopTwoTone, CarTwoTone, StarTwoTone, PhoneTwoTone } from '@ant-design/icons'

import shelby from '../assets/images/shelby.svg'

const weeklyPromotion = [
  { id: 1, label: 'Lunes', src: shelby, discount: '10 %' },
  { id: 2, label: 'Martes', src: shelby, discount: '10 %' },
  { id: 3, label: 'Miercoles', src: shelby, discount: '10 %' },
  { id: 4, label: 'Jueves', src: shelby, discount: '10 %' },
  { id: 5, label: 'Viernes', src: shelby, discount: '10 %' },
  { id: 6, label: 'Sábado', src: shelby, discount: '10 %' },
]

const monthlyPromotion = [
  { id: 1, label: 'Lunes', discount: '10 %' },
  { id: 2, label: 'Martes', discount: '10 %' },
  { id: 3, label: 'Miercoles', discount: '10 %' },
  { id: 4, label: 'Jueves', discount: '10 %' },
  { id: 5, label: 'Viernes', discount: '10 %' },
  { id: 6, label: 'Sábado', discount: '10 %' },
]

const menuData = [
  { key: '', icon: ShopTwoTone, text: 'Inicio' },
  { key: 'services', icon: CarTwoTone, text: 'Servicios' },
  { key: 'promotions', icon: StarTwoTone, text: 'Promocion' },
  { key: 'contact', icon: PhoneTwoTone, text: 'Contacto' },
]

export { weeklyPromotion, monthlyPromotion, menuData }
