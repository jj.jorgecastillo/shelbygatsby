import React from 'react'

import LayoutShelby from '../Components/LayoutShelby'

import Promos from '../Components/Promos'

const Services = () => (
  <LayoutShelby>
    <Promos />
  </LayoutShelby>
)

export default Services
