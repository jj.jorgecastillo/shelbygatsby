import React from 'react'

import LayoutShelby from '../Components/LayoutShelby'

import Promos from '../Components/Promos'

const Promotions = () => (
  <LayoutShelby>
    <Promos />
  </LayoutShelby>
)

export default Promotions
