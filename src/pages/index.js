import React from 'react'

import LayoutShelby from '../Components/LayoutShelby'

import MainShelby from '../Components/MainShelby'

const IndexPage = () => (
  <LayoutShelby>
    <MainShelby />
  </LayoutShelby>
)

export default IndexPage
