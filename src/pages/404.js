import React from 'react'

import { Link } from 'gatsby'

import { Result, Button } from 'antd'

import LayoutShelby from '../Components/LayoutShelby'

const NotFoundPage = () => (
  <LayoutShelby>
    <Result
      status="404"
      title="404"
      subTitle="Lo sentimos, la página que visitaste no existe."
      extra={
        <Link to="/">
          <Button type="primary">Volver al inicio</Button>
        </Link>
      }
    />
  </LayoutShelby>
)

export default NotFoundPage
