import React from 'react'

import LayoutShelby from '../Components/LayoutShelby'

import { Result, Button } from 'antd'

const ResultPage = () => (
  <LayoutShelby>
    <Result
      status="success"
      title="Recibimos tu email"
      subTitle="Gracias por contactarnos, pronto te respondemos."
      extra={[
        <Button type="primary" key="console">
          Ver servicios
        </Button>,
        <Button key="buy">Ver promociones</Button>,
      ]}
    />
  </LayoutShelby>
)

export default ResultPage
